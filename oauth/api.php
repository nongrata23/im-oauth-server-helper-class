<?php

// Проверяем входящие переменные (допустимы и GET и POST):
$inputs = filter_input_array(INPUT_GET, array(
    'method' => FILTER_SANITIZE_STRING,
    'access_token' => FILTER_SANITIZE_STRING,
    'client_id' => FILTER_SANITIZE_STRING,
    'client_secret' => FILTER_SANITIZE_STRING,
    'user_id' => FILTER_SANITIZE_STRING
));
extract($inputs);

if (!$access_token || !$client_id || !$client_secret) {
    // Здесь, конечно, надо провести более тщательную проверку этих переменных.
    die("Доступ запрещен!");
}

// Если User ID не был передан через параметры, то надо вычислить его из Access Token,
// (а если был передан, то проверить):
if (!$user_id) {
    $user_id = 1;
}

// Ну и далее в зависимости от запрошенного метода API должно выполнить какое-то действие и вернуть результат
// в формате JSON.
$json = new stdClass();

switch ($method) {
    case "users.get":
        $user = new stdClass();
        $user->id = 1;
        $user->email = "mail@example.com";
        $user->first_name = "Илья";
        $user->second_name = "Вячеславович";
        $user->last_name = "Моисеев";

        $json->response = array($user);
        break;
    default:
        $json->error = array(
            'error' => "Не выбран метод",
            'error_description' => "Выберите метод для доступа к API."
        );
        break;
};

header('Content-type:application/json; charset=utf-8');
die(json_encode($json, JSON_UNESCAPED_UNICODE));