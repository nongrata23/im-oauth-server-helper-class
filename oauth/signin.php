<?php

$postData = file_get_contents('php://input');
$data = json_decode($postData, true);

$resJSON = array();

if (!is_array($data) || !array_key_exists("login", $data) || !array_key_exists("password", $data)) {
    $resJSON = array(
        'access' => false,
        'error' => "Please, fill in all fields."
    );
} else {
    $login = $data['login'];
    $passw = $data['password'];

    // TODO: Check user login and password!
    if ($login != '12345' || $passw != "12345") {
        $resJSON = array(
            'access' => false,
            'error' => "Wrong login or password."
        );
    } else {
        // TODO: Set real authorization cookie!
        setcookie("access", "true");
        $resJSON = array('access' => true);
    }
}

header('Content-type:application/json; charset=utf-8');
die(json_encode($resJSON));