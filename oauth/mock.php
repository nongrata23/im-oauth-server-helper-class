<?php

function getClientInfoMock(int $client_id)
{
    return array(
        'client_id' => $client_id,
        'client_host' => "auth-server.localhost"
    );
}