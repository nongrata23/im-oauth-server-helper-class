<?php
require_once('oauth2.php');
require_once('OAuthException.php');

use Iskra\OAuth\OAuth2 as OAuth2;
use Iskra\OAuth\OAuthException as OAuthException;

try {
    $oauth = new OAuth2();
    $redirect_uri = $oauth->getRedirectURI();
    // Check if user is authenticated:
    if (!$oauth->isAuthenticated()) throw new OAuthException(
        "User is not signed in",
        OAuthException::OAuth_UnauthorizedClient,
        $redirect_uri);
    // Check response type:
    $response_type = $oauth->getResponseType();
    if ($response_type == "code") {
        $query = http_build_query(array(
            'code' => $oauth->getCode()
        ));
        header("Location: " . $redirect_uri . "?" . $query);
    } elseif ($response_type == "token") {
        $query = http_build_query($oauth->getTokens());
        header("Location: " . $redirect_uri . "#" . $query);
    }
    exit();
} catch (OAuthException $exc) {
    $redirect_uri = $exc->getRedirect();
    $json = json_encode(array(
        'error' => $exc->getTitle(),
        'error_description' => $exc->getMessage()
    ));

    if (empty($redirect_uri)) {
        header('Content-type:application/json; charset=utf-8');
        die($json);
    }
    $query = http_build_query(array(
        'error' => $exc->getTitle(),
        'error_description' => $exc->getMessage()
    ));
    header("Location: " . $redirect_uri . "?" . $query);
    header('Content-type:application/json; charset=utf-8');
    die($json);
}