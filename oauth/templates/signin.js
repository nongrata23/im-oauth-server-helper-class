$(document).ready(function () {
    // Send message button:
    $("#btnSubmit").click(function (event) {
        event.preventDefault();

        txtLogin = $("#inputLogin").val();
        txtPassw = $("#inputPassword").val();

        if (txtLogin.length > 2 && txtPassw) {
            req = {
                login: txtLogin,
                password: txtPassw
            };

            console.log(req);

            $.ajax({
                url: 'signin.php',
                dataType: 'json',
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(req),
                processData: false,
                success: function (resJSON, textStatus, jQxhr) {
                    if (resJSON.access) {
                        location.reload();
                    } else {
                        console.log(resJSON.error);
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log('oops');
                }
            });
        }
    });
});