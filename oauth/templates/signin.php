<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>ЭИОС: Вход в систему</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="templates/signin.css" rel="stylesheet">
</head>
<body class="text-center">
<form class="form-signin">
    <img class="mb-4" src="templates/volgmed-logo.png" alt="" width="200" height="200">
    <h1 class="h3 mb-3 font-weight-normal">ЭИОС: Вход в систему</h1>
    <label for="inputLogin" class="sr-only">Логин</label>
    <input type="email" id="inputLogin" class="form-control" placeholder="Логин" required autofocus>
    <label for="inputPassword" class="sr-only">Пароль</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Пароль" required>
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> Запомнить меня
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" id="btnSubmit">Войти</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2019</p>
</form>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="templates/signin.js"></script>
</body>
</html>