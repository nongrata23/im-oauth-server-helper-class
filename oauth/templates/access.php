<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>ЭИОС: Вход в систему</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="templates/signin.css" rel="stylesheet">

    <script type="text/javascript" language="javascript">// <![CDATA[
        function allow() {
            location.href = "<?php echo $linkAllow ?>";
            return false;
        }

        function cancel() {
            location.href = "<?php echo $linkCancel ?>";
            return false;
        }

        // ]]></script>
</head>
<body class="text-center">
<form class="form-signin">
    <img class="mb-4" src="templates/volgmed-logo.png" alt="" width="200" height="200">
    <h1 class="h3 mb-3 font-weight-normal">ЭИОС: Вход в систему</h1>
    <p>Приложение запрашивает доступ к вашему аккаунту.</p>
    </div>
    <button class="btn btn-lg btn-success btn-block" onclick="return allow();">Разрешить</button>
    <button class="btn btn-lg btn-default btn-block" onclick="return cancel();">Отмена</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2019</p>
</form>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="templates/signin.js"></script>
</body>
</html>