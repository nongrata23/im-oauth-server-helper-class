<?php
require_once('oauth2.php');
require_once('OAuthException.php');

use Iskra\OAuth\OAuth2 as OAuth2;
use Iskra\OAuth\OAuthException as OAuthException;

// Mock functions. Only for 'dev' mode!
require_once('mock.php');

try {
    $oauth = new OAuth2();
    $oauth->authorize();

    // Now we can use redirect for sending messages:
    $redirect_uri = $oauth->getRedirectURI();

    // Check if user is already authenticated:
    if ($oauth->isAuthenticated()) {
        // Generate href for cancel button:
        $linkCancel = $redirect_uri . "?" . http_build_query(array(
                'error' => OAuthException::OAuth_AccessDenied,
                'error_description' => "Authorization cancelled by user"
            ));
        // Generate href for cancel button:
        $linkAllow = "allow.php";
        require_once('templates/access.php');
    } else {
        require_once('templates/signin.php');
    }
} catch (OAuthException $exc) {
    $redirect_uri = $exc->getRedirect();
    $json = json_encode(array(
        'error' => $exc->getTitle(),
        'error_description' => $exc->getMessage()
    ));

    if (empty($redirect_uri)) {
        header('Content-type:application/json; charset=utf-8');
        die($json);
    }
    $query = http_build_query(array(
        'error' => $exc->getTitle(),
        'error_description' => $exc->getMessage()
    ));
    header("Location: " . $redirect_uri . "?" . $query);
    header('Content-type:application/json; charset=utf-8');
    die($json);
}