<?php

declare(strict_types=1);

namespace Iskra\OAuth;

use \Exception as Exception;
use \Throwable as Throwable;

/**
 * Class OAuthException
 * @package Iskra\OAuth
 */
class OAuthException extends Exception
{
    const OAuth_InvalidRequest = "invalid_request";
    const OAuth_UnauthorizedClient = "unauthorized_client";
    const OAuth_AccessDenied = "access_denied";
    const OAuth_Unsupported = "unsupported_response_type";
    const OAuth_InvalidScope = "invalid_scope";

    protected $title;
    protected $redirect;

    public function __construct(
        string $message = "",
        string $title = self::OAuth_InvalidRequest,
        string $redirect = "",
        int $code = 0,
        Throwable $previous = null)
    {
        $this->title = $title;
        $this->redirect = $redirect;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getRedirect()
    {
        return $this->redirect;
    }
}