<?php

declare(strict_types=1);

namespace Iskra\OAuth;

/**
 * Class OAuth2
 * The class contains a number of auxiliary parameters and methods for creating
 * an authorization server according to the OAuth 2.0 standard.
 *
 * ATTENTION! The class allows you to implement authorization on third-party resources through this server.
 * But it is necessary to add methods of authorization of the user on this server.
 * (Marked in the comments as "TODO").
 *
 * @package Iskra\OAuth
 * @author Ilya V. Moiseev <ilyamoiseev@inbox.ru>
 */
class OAuth2
{
    protected $client_id = 0;
    protected $client_host = "";
    protected $redirect_uri = "";
    protected $scope;
    protected $response_type = "code";
    protected $state = "";

    /**
     * The main task of the constructor is to restore the previous state of class parameters
     * if it has already been initialized earlier in the current session.
     * @version 29.01.2019
     */
    public function __construct()
    {
        // Start session:
        session_start();
        // Try to load previous state:
        $this->setData($this->loadSessionData());
    }

    /**
     * The method sets the values of protected class parameters according to the associative array of values
     * passed as a parameter. The method also saves these values (with "oauth_" prefix) in PHP session.
     * @param array $data
     * @version 28.01.2019
     */
    protected function setData(array $data)
    {
        foreach ($data as $key => $value) {
            if (!is_null($value) && property_exists($this, $key)) {
                $this->$key = $value;
                $sessionKey = "oauth_" . $key;
                $_SESSION[$sessionKey] = $value;
            }
        }
        // Convert some values for usability:
        $this->client_id = intval($this->client_id);
        $this->scope = (!is_array($this->scope) && !empty($this->scope)) ?
            $this->scope = explode(" ", strval($this->scope)) : $this->scope;
    }

    /**
     * The method looks for class parameters in the $_SESSION array (prefixed with "oauth_")
     * and returns an associative array with the found parameters.
     * @return array
     * @version 29.01.2019
     */
    protected function loadSessionData(): array
    {
        $data = array();
        foreach (get_object_vars($this) as $key => $value) {
            $sessionKey = "oauth_" . $key;
            if (array_key_exists($sessionKey, $_SESSION)) $data[$key] = $_SESSION[$sessionKey];
        }
        return $data;
    }

    /**
     * The method starts the user authorization procedure on a third-party resource through this server.
     * It parses the data received through the GET request, then gets the client data from the database.
     * The method then validates the client ID and domain for the redirect.
     * @throws OAuthException
     * @version 29.01.2019
     */
    public function authorize()
    {
        // Get parameters from URI query and set them to class:
        $this->setData($this->getURIQuery());
        // Get information about client:
        $this->setData($this->getClientInfo());

        // Check client:
        if (intval($this->client_id) == 0)
            throw new OAuthException("Invalid client ID", OAuthException::OAuth_InvalidRequest);

        // Check host:
        if ($this->client_host != parse_url($this->redirect_uri, PHP_URL_HOST))
            throw new OAuthException("Invalid client host value", OAuthException::OAuth_InvalidRequest);
    }

    /**
     * After basic validation, the method returns an associative array of parameters
     * received from the client via a GET request.
     * @return array
     * @version 28.01.2019
     */
    protected function getURIQuery(): array
    {
        $args = array(
            'client_id' => FILTER_VALIDATE_INT,
            'redirect_uri' => FILTER_SANITIZE_URL,
            'scope' => FILTER_SANITIZE_STRING,
            'response_type' => FILTER_SANITIZE_STRING,
            'state' => FILTER_SANITIZE_STRING
        );
        $input = filter_input_array(INPUT_GET, $args);
        return (is_array($input)) ? $input : array();
    }

    /**
     * This method is used for loading Client data from DB.
     * @return array
     * @version 28.01.2019
     */
    protected function getClientInfo(): array
    {
        // TODO: Replace this mock code with data from DB!
        return array(
            'client_id' => $this->client_id,
            'client_host' => "auth-server.localhost"
        );
    }

    /**
     * An important method is to verify that a user who wants to use OAuth is authenticated on current server.
     * @return bool
     * @version 29.01.2019
     */
    public function isAuthenticated(): bool
    {
        // TODO: Replace this code! Check authorization!
        return boolval(filter_input(INPUT_COOKIE, "access"));
    }

    /**
     * The method is a getter. Returns the value of the protected parameter "redirect_uri".
     * @return string
     * @throws OAuthException
     * @version 29.01.2019
     */
    public function getRedirectURI(): string
    {
        if (empty($this->redirect_uri))
            throw new OAuthException(
                "Invalid redirect URI",
                OAuthException::OAuth_InvalidRequest
            );
        return $this->redirect_uri;
    }

    /**
     * @return string
     * @version 30.01.2019
     */
    public function getCode(): string
    {
        // TODO: Replace this with actual code generator!
        return md5(strval(time()));
    }

    /**
     * @param string $code
     * @param string $state
     * @return bool
     */
    public function checkCode(string $code, string $state = ""): bool
    {
        // TODO: Replace this with actual code check function!
        // You should check code and optionally state:
        if ($state != $this->state) return false;
        return true;
    }

    /**
     * @return array
     * @version 30.01.2019
     */
    public function getTokens(): array
    {
        // TODO: Replace this with actual tokens generator!
        $tokens = array(
            'refresh_token' => 'b45529ac9bf6b32be761975c043ef9e3',
            'access_token' => 'b6442ed12223a7d0b459916b8ea03ce5',
            'token_type' => 'bearer'
        );
        // Note, that you must clear "code" and session data after generating tokens, to prevent its reuse.
        $this->cleanSession();

        return $tokens;
    }

    /**
     * Clean all authorization session data.
     * @return bool
     * @version 29.01.2019
     */
    protected function cleanSession(): bool
    {
        foreach (get_object_vars($this) as $key => $value) {
            $sessionKey = "oauth_" . $key;
            if (array_key_exists($sessionKey, $_SESSION)) unset($_SESSION[$sessionKey]);
        }
        return true;
    }

    /**
     * The method is a getter. Returns the value of the protected parameter "response_type".
     * @return string
     * @version 30.01.2019
     */
    public function getResponseType(): string
    {
        return $this->response_type;
    }
}