<?php
require_once('oauth2.php');
require_once('OAuthException.php');

use Iskra\OAuth\OAuth2 as OAuth2;
use Iskra\OAuth\OAuthException as OAuthException;

try {
    $oauth = new OAuth2();

    // Check code:
    $code = strval(filter_input(INPUT_POST, "code", FILTER_SANITIZE_STRING));
    $state = strval(filter_input(INPUT_POST, "state", FILTER_SANITIZE_STRING));

    if ($oauth->checkCode($code, $state)) {
        throw new OAuthException(
            "Wrong code",
            OAuthException::OAuth_InvalidRequest
        );
    }

    // return tokens:
    header('Content-type:application/json; charset=utf-8');
    echo json_encode($oauth->getTokens());

    exit();
} catch (OAuthException $exc) {
    $json = json_encode(array(
        'error' => $exc->getTitle(),
        'error_description' => $exc->getMessage()
    ));

    header('Content-type:application/json; charset=utf-8');
    die($json);
}