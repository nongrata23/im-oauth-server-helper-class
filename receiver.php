<?php
/**
 * A sample page of a third-party application that accepts a temporary code and sends a POST request
 * to receive authorization tokens.
 */

header('Content-Type: text/html; charset=utf-8'); // Принудительно устанавливаем кодовую страницу UTF-8.

$code = filter_input(INPUT_GET, "code", FILTER_SANITIZE_STRING);
if (!is_null($code)) {
    $url = "https://iskra.volgmed.ru/av/token.php";
    $params = array(
        'grant_type' => "authorization_code",
        'client_id' => "testclient1",
        'client_secret' => "testpass",
        'code' => $code,
        'redirect_uri' => "http://oauth.localhost/receiver.php",
        'state' => "fdkdf889gfd89789743ui"
    );
    $result = @file_get_contents($url, false, stream_context_create(array(
        'http' => array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)
        )
    )));
    if (!$result) {
        echo "Не удалось получить код для запроса токенов авторизации.";
    } else {
        $result = json_decode($result);
        // Сохраняю Access Token в Cookies:
        setcookie("access_token", $result->access_token, time() + intval($result->expires_in));
        header("Location: index.php");
        echo "Код для запроса токенов авторизации получен успешно. Запрашиваю access и refresh токены методом POST.";
        var_dump($result);
    }
} else {
    // In case of an error - information about it is contained in the $_GET array.
    echo "Не удалось получить код для запроса токенов авторизации. Возвращенная ошибка:";
    var_dump($_GET);
}