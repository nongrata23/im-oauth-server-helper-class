<?php
/**
 * Sample page of a third-party application containing the button that triggers the authorize a user
 * on a third-party resource via the authorization server, using the "OAuth2.0" specification.
 */

header('Content-Type: text/html; charset=utf-8'); // Принудительно устанавливаем кодовую страницу UTF-8.

$access = filter_input(INPUT_COOKIE, "access_token");

if (!$access) {
    // Если нет cookies с access_token, то вызываем процедуру OAuth авторизации:
    $query = array(
        // Client ID - ID of web site or app, that requires user to authorize.
        'client_id' => 'testclient1',
        // The URI by which the server will return authorization tokens or an error message.
        'redirect_uri' => "http://oauth.localhost/receiver.php",
        // Sample list of requested permissions separated by space.
        'scope' => "email",
        // Response type:
        // - "code" ("Authorization code flow" for web sites)
        // - "token" ("Implicit flow" for mobile apps or JS).
        'response_type' => "code",
        // Random string for each user added for security purposes.
        'state' => "fdkdf889gfd89789743ui"
    );
    $url = "https://iskra.volgmed.ru/av/index.php?" . http_build_query($query);
} else {
// Если есть cookies c access_token, то обращаемся к API и вытягиваем email, ФИО.
// Возвращаемый результат должен быть массивом объектов типа пользователь (так написано в руководстве ВК).
    $url = "http://oauth.localhost/oauth/api.php?";
    $params = array(
        'method' => "users.get",
        'access_token' => $access,
        'client_id' => 'testclient1',
        'client_secret' => "testpass"
    );
    $result = @file_get_contents($url . http_build_query($params));
    if (!$result) {
        echo "Не удалось обратиться к api И.С.К.Р.А.";
    } else {
        $result = json_decode($result)->response;
        if (!empty($result))
            $name = $result[0]->first_name . " " . $result[0]->last_name;
        else $name = "Unknown";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <title>Вход в ЕИОС</title>
</head>
<body>
<?php if (!$access) { ?>
    <div style="display: flex; flex-direction: column">
        <div style="display: block;"><a href="<?php echo $url ?>">Authorize Me</a></div>
    </div>
<?php } elseif (!empty($name)) { ?>
    <div style="display: flex; flex-direction: column">
        <div style="display: block;">Здравствуйте, <?php echo $name ?></div>
    </div>
<?php } ?>

</body>
</html>
